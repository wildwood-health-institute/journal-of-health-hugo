---
title: "Bruschetta Di Melazana (Eggplant)"
date: 2013-12-27T18:47:00
draft: false
image: bruschetta-di-melazana-eggplant.jpg
categories: [recipes]
ingredients:
    - 1 medium eggplant, peeled and chopped into cubes
    - 1 small red pepper
    - 3 cloves garlic, minced
    - Olive oil
    - Salt
    - Dried oregano
    - 12 slices Italian bread or baguette toast (preferably whole wheat)
instructions:
    - Peel and chop the eggplant into small cubes.
    - Place in a colander and sprinkle with salt. Let drain for 20 minutes.
    - Squeeze to remove the liquid and reserve.
    - To roast the pepper, coat it in olive oil and bake under broil. Keep turning it until the skin is burnt on all sides.
    - Place in a paper bag or wrap in waxed paper and let cool.
    - Remove the jacket and the seeds and set aside.
    - In a skillet, heat the olive oil and the garlic, add the eggplant, chopped roasted pepper and cook for 5 minutes stirring constantly.
    - Season with oregano. Allow to cool.
---
Try this delicious recipe to not only make your taste buds happy, but to help you get to looking and feeling better too! This great recipe incorporates eggplant which has so many health and beauty boosting properties! This great recipe can be found along with many others in our latest issue (Volume 30, Number 3)!

**Feel Better!**  
Just to name a few of its many benefits, eggplant is an excellent source of fiber which aids in (colon) cancer prevention and keeps your digestive system running smoothly! Containing antioxidants such as nasunin and manganese, eggplant aids in lowering blood cholesterol and promotes antimicrobial and antiviral activity. This low calorie, no fat and highly nutritious food definitely will help you to feel better!

**Look Better!**  
Since eggplant is low calorie yet high in fiber and nutrients, it helps you with weight loss and control! The high fiber content helps to fill you up faster, and since it takes longer to digest, you stay full longer, preventing you from being tempted to snack in between meals! Eggplant is also high in water content, as are our bodies! This high water content help to keep us hydrated aiding in healthy hair, skin and nails! So try some eggplant and get to looking better too!

{{< recipe >}}

Notes
Serve on toast with olive oil.
