---
title: "Sweet Sweet Sweet"
date: 2014-01-02T11:39:00
draft: false
image: sweet-sweet-sweet.jpg
categories: [articles]
---
Most humans have a natural affinity for sweets. But it has not always been as easy to satisfy as it has been for the last 200 years since the invention of refined sugar. Before then, anyone wishing to eat something sweet only had two options: honey or fruit (fresh or dried). In either case, satisfying the desire in moderation was healthful.

Unfortunately, the situation today is very different. Although it is still completely possible to satisfy this desire with honey or fruit, few choose this option. The majority, particularly children, prefer pastries, chocolates, and other sweetened foods.

Let us look at various sweeteners, as not all of them were “created equal.”

## Fruits

Although they are not sweeteners in the strict sense of the word, sweet fruits represent the most healthful means of consuming sugar.

Fruits, in addition to natural sugars, contain:

* Phytochemicals as well as vitamins and minerals that facilitate the metabolism of sugar, transforming it into energy.
* Fiber slows the absorption of sugars thus less insulin is required. The less insulin is secreted, the less fat is produced (since one of insulin’s effects is promotion of lipogenesis, synthesis of lipids or fats in the body).
* Due to all of this, the natural “intrinsic” sugar found in fruit has two great advantages over “extrinsic” sugar added to foods, particularly if those foods are refined: It is better tolerated by diabetics, and it is less fattening.

## Molasses

Molasses has significant nutritional value because the minerals, trace minerals, and vitamins are not removed, but some sugars are removed by crystallization and centrifugation. It is useful for iron, calcium, magnesium, and vitamin B6, and it is used therapeutically for iron deficiency anemia, osteoporosis, skin disorders, brittle nails, and hair. Blackstrap molasses is the preferred kind to use.

## Honey

Honey is the oldest known sweetener and is attributed with the most healing properties.

* Moderate use of honey provides a natural, easy-todigest sweetener.
* It contains fructose and glucose and some vitamins and minerals, like calcium and phosphorus.
* It can be kind to dental enamel and has antiseptic properties from its content of inhibins which can even attack staph and diphtheria bacilli when applied to skin surfaces.
* It has some laxative action. Its antibacterial properties can assist in care of bowel infections.
* It can be used for respiratory conditions.
* When used moderately, it is supportive to the liver to help restore and build up glycogen.
* When applied medicinally to the skin, it can facilitate the healing of wounds, ulcers, and even burns.
* Can be used as a gargle solution to help relieve throat irritations.

### Drawbacks of Honey

The key drawback is overuse. Honey should be used with great caution in diabetes. Uncommonly, allergens can sneak into honey, manifesting themselves in itching and in mouth or digestive disorders. Knowing and trusting your honey source can prevent rare negative experiences.

## Maple Syrup

Maple syrup contains 67.2% sugars, principally saccharose, and a certain amount of minerals (calcium, magnesium, iron, potassium, and particularly zinc). It provides virtually no vitamins. It is a healthful sweetener, but of less nutritional value than molasses.

## Sugar

Chemically, sugars are either mono- or disaccharide carbohydrates. The term common sugar (or table sugar) refers to a white, refined product extracted from sugar cane or sugar beets, which is composed almost entirely of saccharose. After an inadequate breakfast, long intense exercise, or fasting, sugar consumption alleviates fatigue and the feelings of anxiety and unease. It provides quick energy, and it is hypoallergenic. However, it is much better to avoid hypoglycemia with a correct diet based on whole foods which liberate glucose slowly and smoothly over a period of hours in the intestine.

### Drawbacks of Sugar

* Calcium loss. Sugar metabolism requires B-complex vitamins (particularly B1) and minerals (calcium in particular). When sugar (which contains no vitamins or minerals), or refined foods are consumed, the body is forced to use its own reserves, thus risking deficiencies. This is why sugar is referred to as a calcium “thief.”
* Displaces other foods. Sugar satiates and reduces appetite. This leads to a reduction in the consumption of necessary healthful foods.
* Dental caries. All simple sugars promote tooth decay.
* Triglycerides. Sugar consumption increases the level of these fatty substances in the blood, which, in turn, promotes arteriosclerosis and heart disease.
* Obesity. Sugar and sugary foods are major causes of obesity in children and adults.
* Hyperactivity. There is a relationship between sugar consumption and certain behavioral disorders such as hyperactivity or attention deficit (ADHD). It has been shown that sugar combined with additives, particularly food colors, can alter the behavior of certain children.

### Sugar and the Brain

Because sugar raises blood glucose too quickly, the body then produces too much insulin, resulting in an imbalance of energy supply to the brain. When this immoderate stimulus is combined with vitamin and mineral deficiencies, the high-sugar program tends to imbalance brain chemistry, physiology, and behavior.

## Corn Syrup

Corn syrup is derived from cornstarch. Some corn syrups contain a high proportion of fructose, such as high fructose corn syrup (HFSC). Regular consumption of HFSC is associated with non-alcoholic fatty liver disease, increased cholesterol and triglycerides, and hyperuricemia.

## Chemical Sweeteners

These sweeteners are used as additives to foods, giving them a sweet flavor, but supply very little or no nutritional value. Today, the food industry has more than 200 chemical substances. The use of these substances is growing, due to the increasing interest in the developing world in lowering caloric intake. Saccharine, cyclamates, aspartame, and acesulfame K are the most commonly-used chemical sweeteners by the food industry as well as by individuals. They are also the most criticized.
Drawbacks to Chemical Sweeteners

* They provide no nutrients.
* They pose health risks. All synthetic intense sweeteners present some risk of undesirable effects, from nervous disturbances to carcinogenic effects. There is an ADI (Acceptable Daily Intake) for each that should not be exceeded.
* Paradoxical effect. Contrary to what would be expected of these sweeteners, they could produce an increase in appetite, with its accompanying weight gain.

## The Sweet Herb

Stevia (stevia rebaudiana) is used as a no-calorie sweetener. The ground herb is usually refined to a white powder, with its sweetness concentrated. Depending on how it is processed, it can have a bitter aftertaste. Benefits of stevia:

* Provides a sweet taste with no calories
* Does not cause dental caries
* Effective in blood pressure reduction (it acts as a vasodilator)
* Improves glucose tolerance, being beneficial for diabetics

Although no toxic effects of Stevia have been reported, it is still under clinical research. But this sweet herb emerges as a healthful alternative to chemical sweeteners.

High Fructose Corn Syrup (HFCS). From studies thus far, we know that HFCS contributes to metabolic syndrome, dyslipidemia,* impairment of hepatic (liver) insulin sensitivity, lipotoxicity,* oxidative stress, and hyperuricemia.*

The big debate centers on whether HFCS is worse than common table sugar as far as contributing to obesity and other metabolic problems.

Evaporated Cane Juice is a loosely-defined term which can include combinations of sugars such as sucrose, glucose, and fructose. It is less processed than common table sugar, but only by one processing step. Nutritional benefits are very minimal, as evaporated cane juice contains only small amounts of vitamins and trace minerals, and has the same amount of calories as table sugar. Thus, it is, essentially, sugar.

Agave Nectar has been heavily touted due to is lower glycemic index than most other sweeteners. However, other data raises concerns for the safety of this sweetener. Some agave nectars have very high fructose levels, in some cases over 90%, which may make it (in this respect) more problematic than high fructose corn syrup.

The high amounts of fructose in agave may contribute to a host of problems like higher blood levels of free fatty acids, interference with insulin production, and an increase in abdominal fat. High fructose consumption has also been linked to fatty liver, obesity, diabetes, heart disease, hypertension, and metabolic syndrome.

Agave nectar not only contains high levels of fructose, but also saponins, steroid derivatives known to (in some cases) cause diarrhea and vomiting. They have also been linked to miscarriages, making it vitally important that pregnant women abstain from agave nectar. Agave is considered a potential teratogen, meaning that it may disturb fetal development and cause birth defects.

Crystalline Fructose consists of 98% pure fructose and 2% water and trace minerals. Consequently, the growing concerns with fructose as a refined sweetener render this agent suspect as well. (None of this, however, should put fruit itself in a bad light. The naturally-occurring fructose found in fruit is unrefined and is not as quickly absorbed because of the fruits’ fiber. Also, naturally-occuring fructose is bound to enzymes that facilitate its proper metabolism.

### Sources

1. Moreman, C.J.; Smeets, F.W.; Kromhout, D. Dietary risk factors for clinically diagnosed gallstones in middle-aged men. A 25-year follow-up study (the
2. Zutphen study). Ann. Epidemiol., 4: 248-254, 1994.
3. Sailer, D. Does sugar play a role in the development of gastroenterologic diseases (Crohn’s disease, gallstones, cancer)? Z. Ernahrungswiss., 29 (Suppl. 1): 39-44, 1990.
4. Katschinski, B.D.; Logan, R.F.; Edmond, M. et al. Duodenal ulcer and refined carbohydrate intake: a case control study assessing dietary fiber and refined sugar intake [see comments]. Gut, 31: 993-996, 1990.
5. Li, K.C.; Zernicke, R.F.; Barnard, R.J. et al. Effects of a high fat-sucrose diet on cortical bone morphology and biomechanics. Calcified Tissue International, 47: 308-313, 1990.
6. Cornee, J.; Pobel, D.; Riboli, E. et al. A case control study of gastric cancer and nutritional factors in Marseilee, France. Eur. J. Epidemiol., 11: 55-65, 1995.
7. La Vecchia, C.; Franceschi, S.; Dolara, P. et al. Refined sugar intake and the risk of colorectal cancer in humans. Int. J. Cancer, 55: 386-389, 1993.
8. Bostick, R.M.; Potter, J.D.; Kushi, L.H. et al. Sugar, meat and fat intake, and non-dairy risk factors for colon cancer incidence in Iowa women (United States). Causes Control, 5: 38-52, 1994.
9. Lenders, C.M;, Hediger, M.L.; Scholl, T.O. et al. Gestational age and infant size at birth are associated with dietary sugar intake among pregnant adolescents. J. Nutr., 127: 1113-1117, 1997.
