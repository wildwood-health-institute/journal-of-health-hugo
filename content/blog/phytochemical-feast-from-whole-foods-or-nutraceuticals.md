---
title: "Phytochemical Feast From Whole Foods or Nutraceuticals"
date: 2013-12-27T20:49:00
draft: false
image: phytochemical-feast-from-whole-foods-or-nutraceuticals.jpg
categories: [articles]
---
A host of different phytochemicals have been identified in fruits, vegetables, whole grains, nuts, seeds, legumes, and herbs. These phytochemicals possess health-promoting properties useful for the treatment and prevention of chronic diseases, such as cancer, cardiovascular disease, and type 2 diabetes.

Phytochemicals are naturally occurring substances that provide color, texture, flavor, and a defense system for the plants against microorganisms and insects.

These phytochemicals also protect us.  Many studies have shown that a high intake of fruits and vegetables is associated with a substantial reduction in the risk of various cancers, coronary heart disease, and stroke.

For instance, a study from Finland showed that a high intake of fruits, vegetables, and berries was associated with a 35% lower risk of all-cause mortality. Why? These foods contain little fat and salt, are rich sources of folic acid, potassium, magnesium, and fiber, and are known to be excellent sources of health-promoting phytochemicals.

## Carotenoids

A diet rich in carotenoids (such as lutein, zeaxanthin, or beta-carotene) is known to enhance several aspects of immune function in the body.  The carotenoids have significant antioxidant activity to quench free radicals, thus protecting the cell from oxidative damage that may lead to the development of tumor cells.  Persons with a high dietary intake of carotenoids may have substantially reduced risk of cancer.

More than 600 carotenoids exist in plants! These pigments are responsible for bright yellow-orange and red colors of many of the commonly eaten fruits.

In addition to carotenoids, fruits and vegetables are rich in vitamin C, folic acid, and other protective substances such as plant sterols, like ß-sitosterol and stigmasterol which have significantly decreased the incidence and growth of chemically induced colon tumors in animals. Some protease inhibitors in legumes may also suppress the initiation and promotion of cancer.

## Whole Grains

Whole grains are also rich in phenolic compounds such as ferulic acid as well as other health-promoting phytochemicals like flavonoids, lignans, phytosterols, phytates, and tocotrienols. In the Nurses’ Health Study, the risks of coronary heart disease, of stroke, and of type 2 diabetes were reduced about 30-40% in those who had the highest intake of whole grains

Recently, it was also observed that a high intake of whole grains was associated with a 20-50% reduction in the risk of cancer and other lifestyle-related causes of death. Because 90% or more of the phytochemicals in grains are in the bran and germ portion of the kernel, it is important to eat grains that are unrefined with those elements left intact.

Here is a list of foods and herbs the National Cancer Institute designates as having cancer-protective properties:

* Highest anticancer activity: garlic, soybeans, cabbage, ginger, licorice root, and the Brassica vegetables (including carrots, celery, cilantro, caraway, dill, and parsley).
* Modest level of cancer-protective activity: onions, flax, citrus, turmeric, cruciferous vegetables (broccoli, Brussels sprouts, cabbage, and cauliflower), tomatoes, brown rice, and whole wheat.
* A measure of anticancer activity: oats, barley, mints, rosemary, thyme, oregano, sage, basil, cucumber, cantaloupe, and berries.
* Plant foods and herbs have multiple pathways through which they work to protect the body.  For instance, various indole compounds present in cruciferous vegetables activate the enzyme benzopyrene hydroxylase that degrades carcinogens.
* Resveratrol, found in red grape skins, has been shown to reduce the risk of various cancers. Compounds in grapes inhibit breast cancer by suppressing the estrogen-producing enzyme aromatase. Oilseeds, such as sesame seed and especially flax seed, are very rich sources of lignans.  Plant lignans are converted to mammalian lignans by bacterial fermentation in the colon. The lignan metabolites have a structural similarity to estrogens and can bind to estrogen receptors and inhibit the growth of estrogen-stimulated breast cancer.
* Fruits and vegetables along with many of the culinary herbs contain terpenoids, which have anti-cancer properties. Rosemary, sage, oregano, thyme, and other herbs in the Lamiacae family possess strong antioxidative activity. For example, rosemary and sage contain substantial levels of carnosol and ursolic acid (potent antioxidants that possess anti-tumor activity). Ginger contains a dozen phenolic compounds, known as gingerols and diary haptanoids, with potent antioxidant activity. The terpenoids, responsible for the unique flavors of many common herbs and seasonings, inhibit cholesterol synthesis and act as cancer-chemopreventive agents.
* Lycopene, the red pigment in tomatoes, strawberries, and watermelon, is protective against heart disease and prostate cancer.  In the Adventist Health Study, men who consumed tomatoes more than 5 times a week had a 40% lower risk of prostate cancer compared with those men consuming tomatoes less than once a week. In the Health Professionals Study, the risk of prostate cancer was 22% lower and 35% lower in those men consuming 4 to 7 servings per week and more than 10 servings of tomato products per week respectively, compared with those consuming less than 1.5 servings per week.
* The use of garlic is associated with cholesterol reduction, decreased blood clotting, immune enhancement, and a lowered risk of cancer, but which form of garlic caused these benefits? Tablets, capsules, tincture, or aged extracts? Garlic supplements are variable in their effectiveness and depend upon the type of preparation used.  The disease you address will determine the type used.

## Citrus

Some plant foods are really loaded with a variety of phytochemicals.

An orange has about 150 phytochemicals altogether. There are 60 flavonoids present. Other citrus fruits are similarly endowed. These compounds activate the P-450 enzyme system that helps eliminate cancer-causing substances, and inhibit tumor cell growth.

The glucarates in citrus which are found in the albedo (the white skin-like portion underneath the peel) are protective against breast cancer by significantly reducing the incidence and multiplicity of mammary tumors, and delaying appearance of tumors.  And finally, another cancer protective element is limonene, a terpenoid present in the peel (and sometimes the processed juice) of citrus fruits.

## Getting More Phytochemicals

There are four ways by which we could increase our intake of phytochemicals.

* By consuming more foods that are rich in phytochemicals
* Taking a supplement. The physiologically-active components of food can be extracted and formulated into supplements, called nutraceuticals. These supplements are used to deliver a concentrated form of a bioactive agent from food, presented in a non-food matrix. They are often used to enhance a person’s health. The dosage level of nutraceuticals usually exceeds that found in normal food. Some of the nutraceuticals have been shown to be safe, while others are not. Isolated phytonutrients often have a different behavior when isolated away from the food matrix. For example, purified isoflavones have no effect on blood lipids, whereas isoflavone-rich soy beans can significantly lower blood lipid levels. Middle-aged adults with normal cholesterol levels who took a 55 mg isoflavone supplement every day for 8 weeks experienced no changes in either their blood LDL cholesterol or triglyceride levels.

Phytochemicals are usually more concentrated in the outer portions of plants. For example, the outer leaves of lettuce contain from 60 to 460 mg/kg of the flavonoid, quercetin, while the inner leaves, protected from the sun, contain only 3-8 mg/kg. In a similar way, flavonoids are concentrated in the skins of fruit. Phloridzin is found at levels of 80-420 ppm in apple peel while the pulp only has 16-20 ppm of phloridzin. In whole wheat, ferulic acid levels are reported to be 490-521 ppm, while refined wheat has only 38-44 ppm.

## Bioavailability

Flavonoids often exist in plants as glycosides with one or more sugar units attached. The sugar must be removed before the flavonoid can be absorbed.

The efficiency of carotenoid absorption is determined by a number of factors such as the food matrix, food processing, structural differences in carotenoids, and interactions with other food components. The lycopene in tomatoes is 2 to 3 times better absorbed into the bloodstream when the tomatoes have been cooked. Heat-processing induces isomerization of all trans-lycopene to cis-isomeric forms, which have greater bioavailability. Food processing also breaks down cell walls and weakens bonds between lycopene and the food matrix. The bioavailability of carotenoids in vegetables such as carrots and spinach have shown a two-to-fivefold increase when the vegetables are liquefied and heated.

Some have asked about giving people Beta-Carotene supplements rather than increasing foods rich in Beta-Carotene. Supplements should increase serum levels and lower the risk of cancer. However, in six large randomized cancer prevention trials, lasting 4 to 12 years, subjects fed 20 to 50 mg/day of beta-carotene actually experienced a 10 to 50% increase in the risk of cancer – the very opposite of what was expected!

The health benefits of a long-term intake of the antioxidant vitamins C, E and beta-carotene as supplements has been called into question. Recent data from large-scale intervention trials has not been positive. On the other hand, the regular consumption of plant foods that are naturally rich in those vitamins is associated with substantial protection against heart disease and cancer.

## Choose Your Dose Properly

Dosage is also a very important element. A number of culinary herbs that are rich in essential oils provide health-promoting effects. The terpenoids inhibit cholesterol synthesis, and provide protection against cancer and cardiovascular disease. However, large amounts of the extracted oils can be very toxic. Many of the monoterpenes (cineole, anethole, camphor, menthol) can be toxic at high doses. Clearly, if we are taking extracts of plants that have highly concentrated levels of phytochemicals, we run the danger of getting toxic levels. An old adage says that the difference between useful therapy and toxicity may be simply one of dosage!

With such a wide variety of health-promoting phytochemicals available, in plant foods, and the data proving them essential for good health and the reduced risk of chronic ailments such as cardiovascular disease and cancer, knowledgeable people will choose plant foods rather than supplements.
