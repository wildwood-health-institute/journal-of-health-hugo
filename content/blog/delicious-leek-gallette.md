---
title: "Delicious Leek Gallette"
date: 2013-12-27T00:31:00
draft: false
image: delicious-leek-gallette.jpg
categories: [recipes]
description: The tasty Leek Galette is sure to get your tastebuds turning in a tailspin. Take it for a test drive this holiday season and your guests are sure to come back.
ingredients:
    - Ingredients For Crust
    - 1 cup unbleached flour
    - ½ cup whole wheat flour
    - ½ teaspoon salt
    - ½ Tablespoon baking powder
    - ½ cup silken tofu
    - ¼ cup soy sour cream
    - 2 Tablespoon plain soy milk
    - ¼ cup olive oil
    - Ingredients for the filling
    - 2 cups sliced leeks
    - 1 Tablespoon olive oil
    - 2 cups mushrooms, sliced
    - ½ teaspoon salt
    - ¾ cup soy sour cream
    - ½ cup chives, chopped
    - ¼ cup parsley, chopped
instructions:
    - In a bowl, mix flours, salt, and baking powder.Set aside.
    - Blend the tofu, sour cream, milk, and olive oil till very smooth
    - Pour the cream over the flour mixture, and mix very gently.
    - Pour the dough over a floured surface
    - Roll out the dough, wrap it in plastic, and put in the fridge for 20 minutes.
    - Meanwhile, saute the leeks in olive oil till soft.
    - Add mushrooms, and season with salt. Cook for 3 minutes.
    - Put into a bowl, and add sour cream,chives, and parsley.
    - Open the dough 1 inch wider than the pizza pan. Spread the filling over the dough, and fold the edge over the filling.
    - Bake for 30 minutes at 350 F or till golden brown.
---
The tasty Leek Galette is sure to get your tastebuds turning in a tailspin. Take it for a test drive this holiday season and your guests are sure to come back.

{{< recipe >}}
