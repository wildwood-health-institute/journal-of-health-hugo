---
title: "Honey Rice Cookies"
date: 2014-01-03T12:11:00
draft: false
image: honey-rice-cookies.jpg
categories: [recipes]
ingredients: 
    - 1 Cup almond butter
    - ⅓ cup honey
    - 2 teaspoons flax meal mixed in ¼ cup Warm water
    - 2 teaspoons baking powder
    - 2 cups finely-chopped almonds
    - ½ cup cornstarch
    - 1 Cup brown rice flour
instructions: 
    - Preheat oven to 325°F
    - In a large bowl, mix together all ingredients except the flour.
    - Add flour, a bit at a time until a soft dough forms. Place dough between two sheets of plastic wrap, and roll out.
    - Remove top sheet of plastic wrap, and cut the dough into circles.
    - Place on oiled cookie sheet.
    - Bake for 10 to 12 minutes or until golden.
---

{{< recipe >}}
