---
title: "Millet Pudding"
date: 2014-01-03T00:06:00
draft: false
image: millet-pudding.jpg
categories: [recipes]
ingredients: 
    - 2 Cups cooked, warm millet
    - ¼ cup raw cashews (rinsed)
    - ¼ cup honey
    - ½ tsp salt
    - 1 tsp vanilla extract
    - ¾ cup water
instructions:
    - Place 1 cup of the millet and all other ingredients in blender and blend until smooth. Add the second cup of millet and blend again.
    - Layer pudding with fresh fruit and top with fruit garnish.
    - Can be served for breakfast or as a dessert.
---

{{< recipe >}}
