---
title: "Contact"
date: 2019-07-01T15:12:27-04:00
draft: false
schema:
    type: ContactPage
---
The Journal of Health & Healing is located on the campus of Wildwood Lifestyle Center & Hospital.

## Details

* PO Box 129, Wildwood, GA 30757
* (706) 820-7456
* journal@wildwoodhealth.org

## Office hours

* Monday - Thursday 8AM to 6PM
* Fridays 8AM to 1PM
* Closed Monday - Thursday 1PM - 3PM

### Send us an email

{{< contact >}}
