---
title: "FAQ"
date: 2019-06-27T17:03:40-04:00
draft: false
schema:
    "@context": "https://schema.org"
    "@type": "FAQPage"
    mainEntity:
     - 
        "@type": "Question"
        name: "How do I access my Journal using my mobile device?"
        acceptedAnswer:
            "@type": "Answer"
            text: "While using your mobile device, click on the “Digital” tab in the main menu. This will bring you to the list of the latest four issues. Click on any of the four issues and you will be asked to login. Use the email and password used when purchasing your subscription. If you forgot your password you can reset it here."
---

**Q.** How do I access my Journal using my mobile device?

**A.** While using your mobile device, click on the “Digital” tab in the main menu. This will bring you to the list of the latest four issues. Click on any of the four issues and you will be asked to login. Use the email and password used when purchasing your subscription. If you forgot your password you can reset it here.
